/**
 * Copyright 2017 IMONT Technologies
 * Created by romanas on 08/01/2017.
 */
package io.imont.sdk.examples;

import io.imont.lion.Lion;
import io.imont.lion.network.localip.LocalIPNetwork;

public class IPDeviceExample {

    public static void main(String[] args) throws Exception {
        Lion lion = Utils.makeLion("Lion 1");
        LocalIPNetwork network = new LocalIPNetwork();
        lion.registerNetwork("ip", network);

        lion.start();
    }
}
