/**
 * Copyright 2017 IMONT Technologies
 * Created by romanas on 08/01/2017.
 */
package io.imont.sdk.examples;

import io.imont.ferret.client.config.FerretConfiguration;
import io.imont.lion.Lion;
import io.imont.lion.LionBuilder;

import java.nio.file.Files;

public class Utils {
    public static Lion makeLion(final String name) throws Exception {
        FerretConfiguration config = new FerretConfiguration();
        config.setFriendlyName(name);
        config.setBootstrapPort(null);
        config.setBoundIPAddress("");
        config.setRendezvousHost("r.imont.tech");
        config.setRendezvousPort(4444);
        config.setTurnAddress("my.turn.server.com");
        config.setTurnPort(1234);
        config.setTurnUsername("myturnusername");
        config.setTurnPassword("myturnpassword");

        Lion lion = new LionBuilder()
                .ferretConfiguration(config)
                .workDir(Files.createTempDirectory("LionTempDir").toString())
                .build();
        return lion;
    }
}
