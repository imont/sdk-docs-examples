/**
 * Copyright 2017 IMONT Technologies
 * Created by romanas on 08/01/2017.
 */
package io.imont.sdk.examples;

import io.imont.hornet.basic.drivers.BasicBundle;
import io.imont.hornet.configuration.HornetProperties;
import io.imont.hornet.lion.ZigBeeNetwork;
import io.imont.lion.Lion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class ZigBeeExample {

    private static final Logger logger = LoggerFactory.getLogger(ZigBeeExample.class);

    private static final Properties config = new Properties();

    static {
        // name of serial device
        config.put("telegesis.serial.device", "/dev/ttyUSB0");
        // baud rate
        config.put("telegesis.serial.baud", "115200");
    }

    public static void main(String[] args) throws Exception {
        final ZigBeeNetwork zigBeeNetwork = new ZigBeeNetwork(new HornetProperties(config));

        Lion lion = Utils.makeLion("Lion 1");
        lion.registerNetwork("ZigBee", zigBeeNetwork);

        lion.getDriverManager().registerBundle(new BasicBundle());
        // at this point you should start seeing ZigBee traffic in the logs
        lion.start();

        // make sure you've got any drivers you want registered BEFORE you start discovering
        // otherwise devices will not be added

        lion.discover(lion.getFerret().getIdentity().getHashname()).subscribe(dev -> {
            logger.info("Found new device: {}", dev.getAddress());
        });
    }
}
