/**
 * Copyright 2017 IMONT Technologies
 * Created by romanas on 08/01/2017.
 */
package io.imont.sdk.examples;

import io.imont.ferret.client.FerretClient;
import io.imont.lion.Lion;
import io.imont.mole.Consistency;
import io.imont.mole.MoleClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BasicExample {

    private static final Logger logger = LoggerFactory.getLogger(BasicExample.class);

    private static boolean eventSynced;

    public static void main(String[] args) throws Exception {

        Lion lion = Utils.makeLion("Lion 1");
        lion.start();
        Lion lion2 = Utils.makeLion("Lion 2");
        lion2.start();

        FerretClient ferret = lion.getFerret();
        ferret.startDiscovering().subscribe(
                candidate -> {
                    if (candidate.getFriendlyName().equals("Lion 2")) {
                        ferret.offerPair(candidate);
                    }
                }
        );

        while (lion2.getFerret().getMembers().size() == 0) {
            Thread.sleep(1000);
        }

        logger.info("Lion1 joined to Lion2");

        MoleClient mole = lion.getMole();
        MoleClient mole2 = lion2.getMole();

        mole2.subscribeToEvents(event -> {
            logger.info("Second instance received event {} -> {}", event.getKey(), event.getValue());
            eventSynced = true;
        });

        mole.raiseEvent("EXAMPLE_DEVICE_ID", "EXAMPLE_EVENT_KEY", "EXAMPLE_VALUE", null).subscribe(result -> {
            logger.info("Sync status with {} is {}", result.getPeerId(), result.getSyncStatus());
        });

        mole2.getEventLog(Consistency.ONLINE, 100).subscribe(event -> {
            logger.info("Mole2 has event from {}, id {}", event.getId().getPeerId(), event.getId().getUuid());
        });

        while (!eventSynced) {
            Thread.sleep(100);
        }

        logger.info("Synchronised");

        lion.close();
        lion2.close();

        System.exit(0);
    }

}
